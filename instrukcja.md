### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.com/inkscape/inkscape.git
cd inkscape

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

apt build-dep -y inkscape
apt install -y wget

wget -v https://gitlab.com/inkscape/inkscape-ci-docker/raw/master/install_dependencies.sh -O install_dependencies.sh
bash install_dependencies.sh --recommended

git pull --recurse-submodules && git submodule update

mkdir build
cd build
cmake ..

make -j8

```

### Gitlab Docker 

```
####################
    - export SONAR_SCANNER_OPTS="-Xmx4096m"
    
    - sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

    - export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

    - apt update

    - apt build-dep -y inkscape
    - apt install -y wget
    
    - wget -v https://gitlab.com/inkscape/inkscape-ci-docker/raw/master/install_dependencies.sh -O install_dependencies.sh
    - bash install_dependencies.sh --recommended
    - git pull --recurse-submodules && git submodule update

    - mkdir build
    - cd build
    - cmake ..
####################

----- make -j8

```

### QTCreator Includes
```
/usr/include/glibmm-2.4
/usr/include/glib-2.0


```

